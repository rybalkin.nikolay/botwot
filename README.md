
Вводная информация:
Пишем бот для World of Tanks.
Oбучение будет основано на анализе видео записанной игры с ютуба.
Проект исключительно для саморазвития и не будет использоваться в реальных боях.
Изначально бот ничего не знает о характеристиках танков.
Он должен сам научиться их определять, ориентироваться на игровой местности, запоминать карты.
Понимать как ведут себя противники. Определять направление стрельбы,
уворачиваться и производить точные выстрелы.

При обучение я хочу видеть процесс его развития, чему он научился и как идет процесс обучения.

Использовать pyton, нейтронные сети, бот должен саморазвиваться,
исследовать и тестировать новые стратегии и тактики

Требования:
Необходимо реализовать загрузку видео с ютуба.
Видео для обучения должны загружаться динаически из папки raw_data
Обработанные данные должны записываться в processed_data
Необходимо доавить логирование с записью в файл и чтения из файла training_log.log
Шибки логировать в error_log.log

Создать структуру проекта, состоящую из папок data, models, src и results.
Разработать скрипт data_processing.py, который будет отвечать за обработку видео-файлов и сохранение полученных данных в формате pickle в папке processed_data.
Разработать скрипт model_training.py, который будет отвечать за обучение модели на основе обработанных данных и сохранение модели в формате h5 в папке models.
Разработать скрипт bot.py, который будет содержать код бота и использовать обученную модель для принятия решений.
Разработать скрипт main.py, который будет являться точкой входа в проект, загружать видео-файлы из папки raw_data, обрабатывать их с помощью data_processing.py, обучать модель с помощью model_training.py, запускать бота и записывать логи в папку results/logs.
Разработать скрипт для загрузки видео с ютуба и сохранения в папке raw_data.
Реализовать логирование с записью в файл training_log.log и чтения из файла training_log.log и error_log.log.

Структура проекта:

project_folder/
├── data/
│   ├── raw/
│   │   ├── video1.mp4
│   │   └── ...
│   ├─ processed/
│   │    ├── video1_data.pickle
│   │    └── ...
│   └── logs
│       
├── dataset/      
│   ├── test/
│   │   ├── link.jpg
│   │   └── ...
│   ├─ train/
│   │    ├── link.jpg
│   │    └── ...
│   └── val
│       ├── link.jpg
│       └── ... 
│       
├── models/
│   ├── model1.h5
│   ├── model2.h5
│   └── ...
├── src/
│   ├── utils/
│   ├── Application.py
│   ├── model_training.py
│   ├── data_processing.py
│   ├── separate_data.py
│   ├── Bot.py
│   └── main.py
└── results/
    └── logs/
        ├── training_log.log
        └── error_log.log


data/ - папка, которая содержит обработанные данные и файлы для обработки.
dataset/ - папка, которая содержит исходные данные для обучения модели.
models/ - папка, которая содержит обученные модели.
src/ - папка, которая содержит скрипты, необходимые для обработки данных, обучения модели и запуска бота.
results/ - папка, которая содержит результаты работы бота, такие как логи.

Давайте рассмотрим каждый из скриптов:

data_processing.py - этот скрипт отвечает за обработку видео-файлов и сохранение полученных данных в формате pickle в папке processed_data. Он будет использоваться в main.py.

separate_data.py - этот скрипт отвечает за разделение исходных данных на тренировочный, тестовый и валидационный наборы данных. Он будет использоваться в model_training.py.

model_training.py - этот скрипт отвечает за обучение модели на основе обработанных данных и сохранение модели в формате h5 в папке models. Он будет использоваться в main.py.

bot.py - этот скрипт содержит код бота и использует обученную модель для принятия решений.

main.py - этот скрипт является точкой входа в проект, загружает видео-файлы из папки raw_data, обрабатывает их с помощью data_processing.py, обучает модель с помощью model_training.py, запускает бота и записывает логи в папку results/logs.

Application.py - это основной класс для приложения.

utils/ - это папка, которая содержит различные вспомогательные скрипты и модули.

video_downloader.py - этот скрипт отвечает за загрузку видео с ютуба и сохранение в папке raw_data.

training_log.log - это файл, в который будут записываться логи тренировки модели.

error_log.log - это файл, в который будут записываться ошибки.




После обработки видео и сохранения изображений, следующим шагом может быть подготовка данных для обучения модели. Это может включать в себя:

Разделение данных на тренировочный, валидационный и тестовый наборы.
Препроцессинг изображений, такой как изменение размера, нормализация значений пикселей и т.д.
Создание меток или классов для каждого изображения, которые будут использоваться в процессе обучения.
Преобразование изображений и меток в формат, который может быть обработан моделью машинного обучения.
После этого вы можете начать обучение модели на подготовленных данных.

Разделение данных — это процесс разбиения доступных данных на наборы, которые затем используются для обучения, валидации и тестирования модели машинного обучения. Обычно данные разделяют на три набора: обучающий, валидационный и тестовый.

Обучающий набор данных используется для обучения модели, то есть её параметры настраиваются на этом наборе данных.

Валидационный набор данных используется для настройки параметров модели, таких как количество эпох, скорость обучения и других параметров. Этот набор данных помогает улучшить модель и снизить риск переобучения.

Тестовый набор данных используется для проверки точности и эффективности модели после её обучения на обучающем наборе данных и настройки параметров на валидационном наборе данных. Этот набор данных помогает оценить качество модели на новых данных, которые не были использованы во время обучения и настройки параметров.

Размеры наборов данных могут зависеть от конкретной задачи, но обычно используют соотношение 60-80% обучающего набора, 10-20% валидационного набора и 10-20% тестового набора.