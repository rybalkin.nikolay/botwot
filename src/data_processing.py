import os
import cv2

# путь к папке с видео
videos_path = "../data/raw/videos"
# путь к папке для сохранения изображений
frames_path = "../data/processed/frames"

# список расширений файлов видео, которые мы хотим обработать
video_extensions = [".mp4", ".avi", ".mov"]

# функция для обработки каждого видео
def process_video(video_path):
    # получаем имя видео файла без расширения
    video_name = os.path.splitext(os.path.basename(video_path))[0]
    # создаем папку с именем видео файла для сохранения кадров
    os.makedirs(os.path.join(frames_path, video_name), exist_ok=True)
    # загружаем видео
    cap = cv2.VideoCapture(video_path)
    # получаем количество кадров в видео
    total_frames = int(cap.get(cv2.CAP_PROP_FRAME_COUNT))
    # обрабатываем каждый кадр
    for i in range(total_frames):
        # считываем кадр
        ret, frame = cap.read()
        # если кадр не удалось считать, выходим из цикла
        if not ret:
            break
        # сохраняем кадр в соответствующую папку
        frame_path = os.path.join(frames_path, video_name, f"{video_name}_{i}.jpg")
        print(f"{video_name}_{i}.jpg")
        cv2.imwrite(frame_path, frame)
    # освобождаем ресурсы
    cap.release()

# обрабатываем каждое видео в папке
def process_videos():
    for file in os.listdir(videos_path):
        if os.path.splitext(file)[1].lower() in video_extensions:
            video_path = os.path.join(videos_path, file)
            process_video(video_path)
