from pytube import YouTube

def file_loader(id = ""):
    # Создаем объект YouTube с ссылкой на видео
    yt = YouTube("https://www.youtube.com/watch?v=" + f"{id}")

    # Получаем самое высокое разрешение видео
    video = yt.streams.get_highest_resolution()

    # Загружаем видео
    video.download('../data/raw/videos')
