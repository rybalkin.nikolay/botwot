import os
import random
from sklearn.model_selection import train_test_split

# путь к папке с изображениями
images_path = "../data/processed/frames"

# список расширений файлов изображений, которые мы хотим обработать
image_extensions = [".jpg", ".png"]

# путь к папке для сохранения разделенных данных
output_path = "../dataset"

# доля тестовой выборки
test_size = 0.2

# доля валидационной выборки
val_size = 0.2

def separate_data():
    # обход каждого изображения
    images = []
    for root, dirs, files in os.walk(images_path):
        for file in files:
            if os.path.splitext(file)[1].lower() in image_extensions:
                images.append(os.path.join(root, file))

    # разделение на тренировочную, валидационную и тестовую выборки
    train_images, test_images = train_test_split(images, test_size=test_size, random_state=42)
    train_images, val_images = train_test_split(train_images, test_size=val_size/(1-test_size), random_state=42)

    # сохранение разделенных данных
    for mode, images in zip(["train", "val", "test"], [train_images, val_images, test_images]):
        mode_path = os.path.join(output_path, mode)
        os.makedirs(mode_path, exist_ok=True)
        for image in images:
            image_name = os.path.splitext(os.path.basename(image))[0]
            print(f"{mode_path}: {image_name}.jpg")
            # os.symlink(image, os.path.join(mode_path, f"{image_name}.jpg"))
            os.symlink(os.path.abspath(image), os.path.join(mode_path, f"{image_name}.jpg"))


