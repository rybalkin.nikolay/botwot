import sys
from PyQt5.QtWidgets import QApplication, QLabel, QMainWindow, QWidget
from PyQt5.QtGui import QPixmap
from screenshots_classifier import ScreenshotsClassifier

class MainWindow(QMainWindow):
    def __init__(self):
        super().__init__()
        self.setWindowTitle("Screenshots Classifier")
        self.resize(640, 480)
        self.central_widget = QWidget(self)
        self.setCentralWidget(self.central_widget)
        self.image_label = QLabel(self.central_widget)
        self.image_label.setGeometry(10, 10, 300, 300)
        self.result_label = QLabel(self.central_widget)
        self.result_label.setGeometry(320, 10, 300, 300)
        self.screenshots_classifier = ScreenshotsClassifier("screenshots")
        self.screenshots_classifier.load_model("model.h5")

    def update_image(self, img_path):
        pixmap = QPixmap(img_path)
        self.image_label.setPixmap(pixmap)
        self.image_label.setScaledContents(True)

    def classify_image(self, img_path):
        result = self.screenshots_classifier.predict(img_path)
        self.result_label.setText(result)

if __name__ == '__main__':
    app = QApplication(sys.argv)
    window = MainWindow()
    window.show()
    sys.exit(app.exec_())
