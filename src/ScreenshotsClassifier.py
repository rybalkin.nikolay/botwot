import os
import cv2
import numpy as np
from keras.models import Sequential, save_model
from keras.layers import Dense, Flatten, Conv2D, MaxPooling2D

class ScreenshotsClassifier:
    def __init__(self, data_dir, img_size=50, test_size=0.2, epochs=10, batch_size=32):
        self.data_dir = data_dir
        self.categories = os.listdir(data_dir)
        self.img_size = img_size
        self.test_size = test_size
        self.epochs = epochs
        self.batch_size = batch_size
        self.model = None

    def _prepare_data(self):
        data = []
        labels = []
        for category in self.categories:
            category_dir = os.path.join(self.data_dir, category)
            if os.path.isdir(category_dir):
                for file in os.listdir(category_dir):
                    file_path = os.path.abspath(os.path.join(category_dir, file))
                    # Загружаем изображение и приводим его к нужному размеру
                    img = cv2.imread(file_path)
                    if img is None:
                        print(f"Failed to load image: {file_path}")
                        continue
                    img = cv2.resize(img, (self.img_size, self.img_size))
                    # Преобразуем изображение в массив и добавляем его в массив данных
                    data.append(np.array(img))
                    # Добавляем метку класса в массив меток
                    labels.append(self.categories.index(category))
        # Преобразуем массивы данных и меток в numpy массивы
        data = np.array(data)
        labels = np.array(labels)
        return data, labels

    def train(self):
        # Подготавливаем данные
        data, labels = self._prepare_data()

        # Делим данные на тренировочный и тестовый наборы
        from sklearn.model_selection import train_test_split
        X_train, X_test, y_train, y_test = train_test_split(data, labels, train_size=0.8)

        # Создаем нейронную сеть
        self.model = Sequential()
        self.model.add(Conv2D(32, (3, 3), activation='relu', input_shape=X_train.shape[1:]))
        self.model.add(MaxPooling2D((2, 2)))
        self.model.add(Conv2D(64, (3, 3), activation='relu'))
        self.model.add(MaxPooling2D((2, 2)))
        self.model.add(Flatten())
        self.model.add(Dense(64, activation='relu'))
        self.model.add(Dense(len(self.categories), activation='softmax'))

        # Компилируем модель
        self.model.compile(optimizer='adam',
                      loss='sparse_categorical_crossentropy',
                      metrics=['accuracy'])

        # Обучаем модель
        self.model.fit(X_train, y_train, epochs=self.epochs, batch_size=self.batch_size, validation_data=(X_test, y_test))

        # Сохраняем модель
        model_name = 'screenshots_classifier.h5'
        save_model(self.model, model_name)

    def predict(self, img_path):
        if self.model is None:
            print("Model is not trained!")
            return
        img = cv2.imread(img_path)
        if img is None:
            print(f"Failed to load image: {img_path}")
            return
        img = cv2.resize(img, (self.img_size, self.img_size))
        img = np.array(img)
        img = img.reshape(1, self.img_size, self.img_size, 3)
        prediction = self.model.predict(img)
        predicted_class = np.argmax(prediction)
        return self.categories[predicted_class]
