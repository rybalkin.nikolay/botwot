import os
from kivy.app import App
from kivy.uix.button import Button
from kivy.uix.label import Label
from kivy.uix.filechooser import FileChooserListView
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.popup import Popup

from src.data_processing import process_video


class Application(App):
    def __init__(self, *args, **kwargs):
        super(Application, self).__init__(*args, **kwargs)
        self.video_label = None
        self.popup = None

    def build(self):
        # Создание корневого контейнера
        root = BoxLayout(orientation='vertical', spacing=10, padding=10)

        self.video_label = Label(text='', size_hint_y=None, height=40)
        root.add_widget(self.video_label)

        if not self.video_label.text:
            # Создание кнопки выбора видео
            video_button = Button(text='Выбрать видео', size_hint_y=None, height=40)
            video_button.bind(on_press=self.browse_video)
            root.add_widget(video_button)
        else:
            # Создание кнопки запуска обработки видео
            ran_video_processing = Button(text='Обработка выбранного видеофайла', size_hint_y=None, height=40)
            ran_video_processing.bind(on_press=self.ran_video_processing)
            root.add_widget(ran_video_processing)

        # Создание метки для отображения выбранного видео
        self.video_label = Label(text='', size_hint_y=None, height=40)
        root.add_widget(self.video_label)

        # Создание кнопки для запуска обучения
        train_button = Button(text='Обучить модель', size_hint_y=None, height=40)
        train_button.bind(on_press=self.train_model)
        root.add_widget(train_button)

        # Создание кнопки для запуска бота
        bot_button = Button(text='Запустить бота', size_hint_y=None, height=40)
        bot_button.bind(on_press=self.start_bot)
        root.add_widget(bot_button)

        # Создание метки для отображения состояния бота
        self.status_label = Label(text='', size_hint_y=None, height=40)
        root.add_widget(self.status_label)

        return root

    def browse_video(self, instance):
        video_folder = os.path.join(os.path.dirname(__file__), '../data/raw/videos')
        file_chooser = FileChooserListView(path=video_folder)
        file_chooser.bind(on_submit=self.update_video_label)
        file_chooser.filters = ['*.mp4']
        self.popup = Popup(title='Выберите видео', content=file_chooser, size_hint=(0.9, 0.9))
        self.popup.open()

    def ran_video_processing(self, instance, *args):
        process_video(self.video_label.text)

    def update_video_label(self, instance, *args):
        self.video_label.text = instance.selection[0]
        self.popup.dismiss()
        ran_video_processing = Button(text='Обработка выбранного видеофайла', size_hint_y=None, height=40)
        ran_video_processing.bind(on_press=self.ran_video_processing)
        self.root.add_widget(ran_video_processing)

    def train_model(self, instance):
        self.status_label.text = 'Обучение модели'

    def start_bot(self, instance):
        self.status_label.text = 'Запуск бота'

if __name__ == '__main__':
    Application().run()
