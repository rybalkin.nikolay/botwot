# from src.traine import train
from src.Bot import Bot
from src.data_processing import process_videos
from src.file_loader import file_loader
from src.separate_data import separate_data
from src.traine import ScreenshotsClassifier

classifier = ScreenshotsClassifier(data_dir='../dataset', img_size=50, test_size=0.2, train_size=0.8, epochs=10, batch_size=32)

bot = Bot("../models/screenshots_classifier.h5", "../data/raw/videos/video1.mp4")

def main():
    # file_loader("T0sblFztXXE&ab_channel=JOHNNYИЛУЧШИЕБОИWORLDOFTANKS%21")
    # process_videos()
    # separate_data()
    # classifier.train()
    # train()
    # app = Application()
    # app.run()
    # bot.run()


if __name__ == '__main__':
    main()
